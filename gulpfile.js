// /////////////////////
// Required
// /////////////////////

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    bulkSass = require('gulp-sass-bulk-import'),
    autoPrefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber');

var src = {
    scss: 'app/scss/*.scss',
    css: 'app/css',
    html: 'app/*.html'
};

// /////////////////////
// Scripts Tasks
// /////////////////////

gulp.task('scripts', function() {
    gulp.src(['app/js/**/*.js', '!app/js/**/*.min.js'])
        .pipe(plumber())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'));
});

// /////////////////////
// Sass Tasks
// /////////////////////

gulp.task('sass', function() {
    gulp.src('app/scss/main.scss')
        .pipe(bulkSass())
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoPrefixer({
            browsers: ['last 2 versions'],
        }))
        .pipe(gulp.dest('app/css'))
        .pipe(reload({
            stream: true
        }));
});


// /////////////////////
// HTML Task
// /////////////////////

gulp.task('html', function() {
    gulp.src('app/**/*.html')
});

// /////////////////////
// Watch Tasks
// /////////////////////

gulp.task('watch', function() {
    gulp.watch('app/js/**/*.js', ['scripts']);
    gulp.watch('app/scss/**/*.scss', ['sass']);
    gulp.watch('app/**/*.html', ['html']);
});

// /////////////////////
// Sync Task
// /////////////////////

gulp.task('sync', function() {
    browserSync.init({
        server: './app/'
    })
    gulp.watch('app/js/**/*.js', ['scripts']);
    gulp.watch('app/scss/**/*.scss', ['sass']);
    gulp.watch('app/**/*.html', ['html']).on('change', reload);
});

// /////////////////////
// Default Tasks
// /////////////////////
gulp.task('default', ['scripts', 'sass', 'html', 'sync', 'watch'])

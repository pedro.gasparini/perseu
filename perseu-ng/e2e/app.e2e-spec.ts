import { PerseuNgPage } from './app.po';

describe('perseu-ng App', function() {
  let page: PerseuNgPage;

  beforeEach(() => {
    page = new PerseuNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
